﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFH
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            notifyIcon1.Visible = false;
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void Form2_Resize(object sender, EventArgs e)
        {
            notifyIcon1.BalloonTipTitle = "Work From Home Tracker Minimize";
            notifyIcon1.BalloonTipText = "Yopu can control your application here.";
            //notifyIcon1.ContextMenu.MenuItems.Add(new MenuItem("Option 1", new EventHandler(handler_method)));
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult ds = MessageBox.Show("Do you want to exit ?", "Confirm Exit.", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (ds == DialogResult.Yes)
            {
                e.Cancel = false;
               
                try
                {
                    // Work Here for closing
                }
                catch (Exception ex)
                {
                    // MessageBox.Show("No process is running...");
                }
            }
            else
            {
                e.Cancel = true;


            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Stop Code here
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Start Code Here
            notifyIcon1.BalloonTipTitle = "Activity Tracking Started";
            notifyIcon1.BalloonTipText = "Your work is now tracked";
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(500);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
