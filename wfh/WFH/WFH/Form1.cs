﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFH
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String eid = eid_textBox.Text.ToString();
            String pass = pass_textBox.Text.ToString();
            UtilAPI util_api = new UtilAPI();
            String response = util_api.LoginToHRIS(eid, pass);
            // Check login and call API for validation
            //save token and other information if successful
            // else show dialog box
            if (response == "SUCCESS")
            {
                this.Hide();
                var form2 = new Form2();
                form2.Closed += (s, args) => this.Close();
                form2.Show();
            }else
            {
                MessageBox.Show("Login Failed");
            }
        }
    }
}
