﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WFH.Entity;
using WFHScreenShot;
namespace WFH
{
    public partial class Form2 : Form
    {
        #region Global Variable
        public string TokenRes = "";
        public string User = "";
        int timeMin, timeCs, timeSec;
        bool isActive;
        bool eligibility;
        DialogBox m = null;
        bool IsScreenCapture;
        #endregion
        public Form2(string user)
        {
            InitializeComponent();
            notifyIcon1.Visible = false;
            User = user;
            label1.Text = user;
            ResetTime();
            isActive = false;
        }

        #region Eligibility
        private bool Eligibility()
        {
            UtilAPI util_api3 = new UtilAPI();
            string token3 = AppGlobal.TokenRes;
            string responseLogMin3 = util_api3.EligibilityChecking(token3);
            try
            {
                EligibilityEntity logmResponse3 = JsonConvert.DeserializeObject<EligibilityEntity>(responseLogMin3);

            if (logmResponse3.data != null)
            {
                if (logmResponse3.data.status == "approved")
                {
                    if (logmResponse3.code == 200)
                    {
                           AppGlobal.savelog("Eligibility Api Response...: " + logmResponse3.messages[0], true);
                           eligibility = true;
                    }

                    else
                    {
                           AppGlobal.savelog("Log Time Api Response...: " + logmResponse3.messages[0], true);
                    }
                }
                else
                {
                    //MessageBox.Show("You Are Not Eligible For Work From Home! Contact With SSL!");

                    //cmdStart.Enabled = false;
                    //cmdStop.Enabled = false;

                           eligibility = false;
                           AppGlobal.savelog("Not Eligible For WFH  ", true);
                }
            }
            else
            {
                //MessageBox.Show("You Are Not Eligible For Work From Home! Contact With SSL!");

                //cmdStart.Enabled = false;
                //cmdStop.Enabled = false;

                eligibility = false;
                AppGlobal.savelog("Not Eligible For WFH  ", true);
                AppGlobal.savelog("Data Null From Eligible API  ", true);
            }

            }

            catch (Exception ex)
            {
                eligibility= false;
            }

            return eligibility;
        }
        #endregion

        private void ResetTime()
        {
            timeMin = 0;
            timeCs = 0;
            timeSec = 0;
            lblHour.Text = String.Format("{0:00}", 0);
            lblMin.Text = String.Format("{0:00}", 0);
        }




        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void Form2_Resize(object sender, EventArgs e)
        {
            
            if (FormWindowState.Minimized == this.WindowState)
            {
                Notification("Work From Home Tracker", "You can control your application here.", 200);
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(200);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }


        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult ds = MessageBox.Show("Do you want to exit ?", "Confirm Exit.", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (ds == DialogResult.Yes)
            {
                e.Cancel = false;

                try
                {
                    if (isActive)
                    {
                        #region Log Minute
                        String log_update = AppGlobal.LogTime();
                        AppGlobal.savelog("Log Minute Api Response...: " + log_update, true);

                        #endregion

                        #region Stop Timer ScreenShot
                        AppGlobal.savelog("Stopped form toolStrip...", true);
                        String sc = CaptureAndSentScreenShot();
                        Notification("ScreenShot Captured", sc, 200);
                        #endregion



                        #region Post Status
                        AppGlobal.savelog("Post status stopped cmdStop_Click", true);
                        String ps = AppGlobal.PostStatus("stop");
                        #endregion
                        AppGlobal.last_mouse_position_X = 0;
                        AppGlobal.last_mouse_position_Y = 0;
                    }
                    timer1.Enabled = false;
                    cmdStart.Enabled = true;
                    cmdStop.Enabled = false;
                    isActive = false;
                    ResetTime();
                }
                catch (Exception ex)
                {

                    //savelog("Exit Application Getting Error: " + ex.ToString(), false);
                }
            }
            else
            {
                e.Cancel = true;


            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Stop Code here
            notifyIcon1.BalloonTipTitle = "Activity Tracking Stopped";
            notifyIcon1.BalloonTipText = "Your work is now stopped tracking";
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(200);
            timer1.Enabled = false;
            cmdStart.Enabled = true;
            cmdStop.Enabled = false;
            isActive = false;
            timer2.Enabled = false;
            #region Stop Timer ScreenShot From ToolStripMenuItem
            AppGlobal.savelog("Stopped form stopToolStripMenuItem_Click...", true);
            String sc = CaptureAndSentScreenShot();
            Notification("ScreenShot Captured", sc, 200);
            #endregion


            #region Post Status
            AppGlobal.savelog("Post status stopped stopToolStripMenuItem_Click", true);
            String ps = AppGlobal.PostStatus("stop");
            #endregion
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Start Code Here
            notifyIcon1.BalloonTipTitle = "Activity Tracking Started";
            notifyIcon1.BalloonTipText = "Your work is now tracked";
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(200);

            timer1.Enabled = true;
            cmdStart.Enabled = false;
            cmdStop.Enabled = true;
            isActive = true;
            timer2.Enabled = true;

            #region Start Timer ScreenShot From ToolStripMenuItem
            AppGlobal.savelog("Stopped form startToolStripMenuItem_Click...", true);
            String sc = CaptureAndSentScreenShot();
            Notification("ScreenShot Captured", sc, 200);
            #endregion

            #region Post Status
            AppGlobal.savelog("Post status stopped startToolStripMenuItem_Click", true);
            String ps = AppGlobal.PostStatus("start");
            #endregion

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isActive)
            {

                #region Log Minute
                String log_update = AppGlobal.LogTime();
                AppGlobal.savelog("Log Minute Api Response...: " + log_update, true);

                #endregion

                #region Stop Timer ScreenShot
                AppGlobal.savelog("Stopped form exitToolStripMenuItem_Click...", true);
                String sc = CaptureAndSentScreenShot();
                Notification("ScreenShot Captured", sc, 200);
                #endregion



                #region Post Status
                AppGlobal.savelog("Post status stopped cmdStop_Click", true);
                String ps = AppGlobal.PostStatus("stop");
                #endregion

                AppGlobal.last_mouse_position_X = 0;
                AppGlobal.last_mouse_position_Y = 0;
            }
            timer1.Enabled = false;
            cmdStart.Enabled = true;
            cmdStop.Enabled = false;
            isActive = false;
            ResetTime();

            Application.Exit();
        }



        public struct POINT
        {
            public int X;
            public int Y;
        }
        public static int _x, _y;
        [DllImport("user32.dll")]
        static extern bool GetCursorPos(out POINT lpPoint);
        static void ShowMousePosition()
        {
            POINT point;
            if (GetCursorPos(out point) && point.X != _x && point.Y != _y)
            {
                //Console.Clear();
               // Console.WriteLine("({0},{1})", point.X, point.Y);
                if (AppGlobal.last_mouse_position_X == point.X && AppGlobal.last_mouse_position_Y == point.Y)
                {
                    // Mouse not moving at all
                }
                else
                {
                    AppGlobal.last_mouse_move = DateTime.Now;
                    AppGlobal.last_mouse_position_X = point.X;
                    AppGlobal.last_mouse_position_Y = point.Y;
                }
                _x = point.X;
                _y = point.Y;
            }
        }

        public static String[] GetFilesFrom(String searchFolder, String[] filters, bool isRecursive)
        {
            List<String> filesFound = new List<String>();
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, String.Format("*.{0}", filter), searchOption));
            }
            return filesFound.ToArray();
        }

        public void Notification(String title, String message, int showTime)
        {
            notifyIcon1.BalloonTipTitle = title;
            notifyIcon1.BalloonTipText = message;
            notifyIcon1.Visible = true;
            notifyIcon1.ShowBalloonTip(showTime);
        }



        //int i = 000000;
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            timer1.Interval = Int32.Parse(AppGlobal.timer);
            AppGlobal.savelog("Timer Start", true);
            label_last.Text = String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now);
            try
            {
                AppGlobal.savelog("Taking screenshot... OS : {0}  " + Environment.OSVersion.Platform, true);
                AppGlobal.savelog("Timer tick form timer1_Tick...", true);
                if (isActive)
                {



                    #region Log Minute
                    String log_update = AppGlobal.LogTime();
                    AppGlobal.savelog("Log Minute Api Response...: " + log_update, true);

                    #endregion

                    if (IsScreenCapture)
                    {
                        String sc = CaptureAndSentScreenShot();
                        Notification("ScreenShot Captured", sc, 200);
                        AppGlobal.savelog("Done Sent sc", true);
                    }

                    //String sc = CaptureAndSentScreenShot();
                    //Notification("ScreenShot Captured", sc, 200);
                    //AppGlobal.savelog("Done Sent sc", true);
                }
                else
                {
                    AppGlobal.savelog("Not active", true);
                }

                #region Log Time Api
                UtilAPI util_api3 = new UtilAPI();
                string token3 = TokenRes;
                string responseLogMin3 = util_api3.LogTime();

                LogTimeEntity logmResponse3 = JsonConvert.DeserializeObject<LogTimeEntity>(responseLogMin3);

                if (logmResponse3.code == 200)
                {
                    //label7.Text =logmResponse3.data.total_logged_time +" min";

                    int a = Convert.ToInt32(logmResponse3.data.total_logged_time);
                    int b = a / 60;
                    String hour = "" + (a / 60);
                    String minute = "" + (a % 60);
                    label7.Text = hour + " Hour " + minute + " Minute";
                    AppGlobal.savelog("Total Logged Time : " + logmResponse3.data.total_logged_time, true);
                    AppGlobal.savelog("Log Time Api Response...: " + logmResponse3.messages[0], true);

                    //if (!IsScreenCapture)
                    //{
                    //    Notification("Logged Time ", " Time Logged Has been sent " + hour + " Hour " + minute + " Minute", 200);
                    //}
                    //IsScreenCapture = false;
                }
                else
                {
                    AppGlobal.savelog("Log Time Api Response...: " + logmResponse3.messages[0], true);
                }
                #endregion

            }
            catch (Exception ex)
            {
                AppGlobal.savelog("Error Getting Screenshot.." + ex.ToString(), false);
            }
            if (isActive)
            {
                timer1.Enabled = true;
            }else
            {
                timer1.Enabled = true;
            }
            AppGlobal.savelog("Timer Enabled True...", true);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (isActive)
            {
                ShowMousePosition();
                int idle_time = AppGlobal.GetIdleTime();
                if (idle_time > AppGlobal.idle_time)
                {
                    if(m !=null)
                    m.Close();

                    if (isActive)
                    {
                        timer1.Enabled = false;
                        cmdStart.Enabled = true;
                        cmdStop.Enabled = false;
                        isActive = false;
                        AppGlobal.savelog("You have been found inactive for long time", true);
                        Notification("Inactive Notification", "You have been found inactive for long time", 500);
                        ResetTime();
                    }
                }
                TimeSpan timeDiff = DateTime.Now - AppGlobal.start_time_global_show;
                Double logTime = timeDiff.TotalMinutes;
                String hour = string.Format(String.Format("{0:00}", (int)(logTime / 60)));
                String minute = string.Format(String.Format("{0:00}", (int)(logTime % 60)));
                //labelCs.Text = String.Format("{0:00}", timeCs);
                lblHour.Text = String.Format("{0:00}", hour);
                lblMin.Text = String.Format("{0:00}", minute);
            }
        }

        private string CaptureAndSentScreenShot()
        {
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory + "\\sc");

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                AppGlobal.savelog("File deleted", true);
            }
            catch (Exception ex)
            {
                AppGlobal.savelog(ex.ToString(), false);
            }


            using (var screen = ScreenshotCapture.TakeScreenshot())
            {
                try
                {
                    String file_name = "sc\\Screenshot" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond + ".jpg";
                    try
                    {
                        screen.Save(Path.Combine(Environment.CurrentDirectory, file_name), ImageFormat.Jpeg);
                    }
                    catch (Exception ex)
                    {
                        Bitmap screen_b = new Bitmap(screen);
                        screen_b.Save(Path.Combine(Environment.CurrentDirectory, file_name), ImageFormat.Jpeg); // This is always successful
                        screen_b.Dispose();
                    }
                    String searchFolder = Environment.CurrentDirectory+"\\sc";

                    var filters = new String[] { "jpg", "jpeg", "png", "gif", "tiff", "bmp", "svg" };
                    var files = Form2.GetFilesFrom(searchFolder, filters, false);
                    if (files.Length>0)
                    {

                        string token = AppGlobal.TokenRes;
                        String img_url = Environment.CurrentDirectory + "\\" + file_name;
                        m = new DialogBox(img_url);
                        m.ShowDialog();

                        bool okButtonClicked = m.OKButtonClicked;

                        if (okButtonClicked)
                        {
                            UtilAPI util_api = new UtilAPI();
                            string response = util_api.FileUpload(token, img_url);

                            FileUploadEntity fupResponse = JsonConvert.DeserializeObject<FileUploadEntity>(response);
                            if (fupResponse.code == 201)
                            {
                                AppGlobal.savelog("File Upload Api Response...: " + fupResponse.messages[0], true);
                                m.Dispose();
                                return "Screen Captured & Sent";
                            }
                            else
                            {
                                AppGlobal.savelog("File Upload Api Response ...: " + fupResponse.messages[0], true);
                                m.Dispose();
                                return "Screen Captured & Failed Sent";
                            }

                        }
                        else
                        {
                            AppGlobal.savelog("Skipping screen shot..", true);
                             m.Dispose();
                            return "Screen Captured & Skipped";
                        }
                       
                    }
                    else
                    {
                        return "Screen Capture Failed";
                    }

                }
                catch (Exception ex)
                {

                    AppGlobal.savelog("Stop Time ScreenShot Error In {using (var screen = ScreenshotCapture.TakeScreenshot())} : " + ex.ToString(), false);
                    return "Screen Capture failed";
                }
            }

        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (isActive)
            {

                #region Log Minute
                String log_update = AppGlobal.LogTime();
                AppGlobal.savelog("Log Minute Api Response...: " + log_update, true);

                #endregion

                #region Exit Application ScreenShot From ToolStripMenuItem
                AppGlobal.savelog("logout form logoutToolStripMenuItem_Click...", true);
                String sc = CaptureAndSentScreenShot();
                Notification("ScreenShot Captured", sc, 200);
                #endregion



                #region Post Status
                AppGlobal.savelog("Post status logout logoutToolStripMenuItem_Click", true);
                String ps = AppGlobal.PostStatus("stop");
                #endregion
                AppGlobal.last_mouse_position_X = 0;
                AppGlobal.last_mouse_position_Y = 0;

            }

            timer1.Enabled = false;
            cmdStart.Enabled = true;
            cmdStop.Enabled = false;
            isActive = false;
            ResetTime();
        }

        private void DrawTime2()
        {
            
            //throw new NotImplementedException();
        }

        private void cmdStart_Click(object sender, EventArgs e)
        {
            if (Eligibility())
            {
                timer1.Enabled = true;
                timer1.Interval = 100;
                cmdStart.Enabled = false;
                cmdStop.Enabled = true;
                isActive = true;
                IsScreenCapture = true;
                AppGlobal.start_time_global_show = DateTime.Now;
                AppGlobal.start_time_global = DateTime.Now;

                #region Start Timer ScreenShot

                #endregion

                #region Post Status
                AppGlobal.savelog("Post status start button cmdStart_Click", true);
                String ps = AppGlobal.PostStatus("start");
                #endregion
            }
            else
            {
                AppGlobal.savelog("You Are Not Eligible For Work From Home! Contact With SSL!", true);
                MessageBox.Show("You Are Not Eligible For Work From Home! Contact With SSL!");
            }
        }

        private void HookManager_MouseMove(object sender, MouseEventArgs e)
        {
            //labelMousePosition.Text = string.Format("x={0:0000}; y={1:0000}", e.X, e.Y);
            AppGlobal.last_mouse_move = DateTime.Now;
        }

        private void HookManager_KeyPress(object sender, KeyPressEventArgs e)
        {
            //textBoxLog.AppendText(string.Format("KeyPress - {0}\n", e.KeyChar));
           // textBoxLog.ScrollToCaret();
            AppGlobal.last_mouse_move = DateTime.Now;
        } 

        private void cmdStop_Click(object sender, EventArgs e)
        {
            if (Eligibility())
            {

                if (isActive)
                {

                    #region Log Minute
                    String log_update = AppGlobal.LogTime();
                    AppGlobal.savelog("Log Minute Api Response...: " + log_update, true);

                    #endregion

                    #region Stop Timer ScreenShot
                    AppGlobal.savelog("Stopped form toolStrip...", true);
                    String sc = CaptureAndSentScreenShot();
                    Notification("ScreenShot Captured", sc, 200);
                    #endregion



                    #region Post Status
                    AppGlobal.savelog("Post status stopped cmdStop_Click", true);
                    String ps = AppGlobal.PostStatus("stop");
                    #endregion

                    AppGlobal.last_mouse_position_X = 0;
                    AppGlobal.last_mouse_position_Y = 0;
                }

                timer1.Enabled = false;
                cmdStart.Enabled = true;
                cmdStop.Enabled = false;
                isActive = false;
                ResetTime();
            }
            else
            {              
                MessageBox.Show("You Are Not Eligible For Work From Home! Contact With SSL!");
            }
        }
           

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void logOutToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void Form2_Click(object sender, EventArgs e)
        {

        }

        
    }
}
