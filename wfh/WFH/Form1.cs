﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WFH.Entity;

namespace WFH
{
    public partial class Form1 : Form
    {
        #region Global Variable
        private string token = "";
        public string Token
        {
            get { return token; }
            set { token = value; }
        }
        #endregion

        public Form1()
        {
            InitializeComponent();
            AppGlobal.TokenRes = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                String eid = eid_textBox.Text.ToString();
                String pass = pass_textBox.Text.ToString();
                UtilAPI util_api = new UtilAPI();
                string response = util_api.LoginToHRIS(eid, pass);

                if (response != "Failed")
                {
                    LoginEntity loginResponse = JsonConvert.DeserializeObject<LoginEntity>(response);

                    if (loginResponse.code == 200)
                    {
                       // AppGlobal.savelog("Save Token :  " + loginResponse.data.token, true);
                        AppGlobal.savelog("Got Token Successful", true);
                        Token = loginResponse.data.token;

                        #region Agent Installation
                        string response3 = util_api.AgentInstalled(Token);
                        AppGlobal.savelog("Got Agent Application Api Response",true);
                        #endregion

                        DateTime dt = DateTime.Now;
                        string started_at_hour = DateTime.Now.ToString("HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                        string user = loginResponse.data.user.name;
                        this.Hide();
                        AppGlobal.TokenRes = Token;
                        var form2 = new Form2(user);
                        form2.Closed += (s, args) => this.Close();
                        form2.Show();

                    }
                    else
                    {
                        MessageBox.Show("Login Failed");
                    }
                }
                else
                {
                    MessageBox.Show("Login Data Invalid");
                }
            }
            catch (Exception ex)
            {
                AppGlobal.savelog(ex.ToString(), false);

            }
        }

        private void eid_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pass_textBox.Focus();
            }
        }

        private void pass_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login_button.PerformClick();
            }
        }
    }
}
