﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFH.Entity;
using WFHScreenShot;

namespace WFH
{
    class AppGlobal
    {
        public static string base_url = "http://apigw.imaladin.com/";

        //public static string base_url = "http://stg-apigw.imaladin.com";

        //http://apigw-saas.employeebuddy.xyz/
        //http://apigw.imaladin.com/   --live

        public static string TokenRes = "";
        public static DateTime start_time_global_show = DateTime.Now;
        public static DateTime start_time_global = DateTime.Now;
        public static DateTime ent_time_global = DateTime.Now;
        public static DateTime last_mouse_move = DateTime.Now;
        public static int last_mouse_position_X = 0;
        public static int last_mouse_position_Y = 0;

        public static int log_minutes = 0;
        public static string timer = "900000";//900000
        public static int idle_time = 28;
        public static string log_path = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
        public static string time_out = "3000";
        public static string script_url_login = AppGlobal.base_url + "/accounts/v1/auth/login";
        public static string script_url_file_uload = AppGlobal.base_url + "/files/v1/upload";
        public static string script_url_log_time = AppGlobal.base_url + "accounts/v1/track/log";
        public static string script_url_agent_installed = AppGlobal.base_url + "accounts/v1/track/mark-agent-installed";
        public static string script_url_post_status = AppGlobal.base_url + "accounts/v1/track/status";
        public static string script_url_looged_time = AppGlobal.base_url + "accounts/v1/track/logged-time";
        public static string script_url_leligibility = AppGlobal.base_url + "accounts/v1/track/eligibility";


        public static int GetIdleTime()
        {
            try
            {
                TimeSpan timeDiff = DateTime.Now - AppGlobal.last_mouse_move;
                Double logTime = timeDiff.TotalMinutes;
                return (int)logTime;
            }
            catch (Exception ex)
            {
                AppGlobal.savelog(ex.ToString(), false);
                return -1;
            }
        }
        public static string LogTime()
        {
            try
            {
                List<string> processList = new List<string>();
                Process[] processes = Process.GetProcesses();
                foreach (Process p in processes)
                {
                    if (!String.IsNullOrEmpty(p.MainWindowTitle))
                    {
                        AppGlobal.savelog("Process Running : " + p.MainWindowTitle, true);
                        //listBox1.Items.Add(p.MainWindowTitle);
                        processList.Add(p.MainWindowTitle);
                    }
                }

                #region Log Minute
                DateTime dt = DateTime.Now;
                string ended_at_hour = DateTime.Now.ToString("HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                UtilAPI util_api2 = new UtilAPI();
                string date = Convert.ToDateTime(DateTime.Now.ToString()).Date.ToString("yyyy-MM-dd");
                DateTime started_at_dt = AppGlobal.start_time_global;
                String started_at = started_at_dt.ToString("HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                DateTime ended_at_dt = DateTime.Now;
                TimeSpan timeDiff = ended_at_dt - started_at_dt;
                Double logTime = timeDiff.TotalMinutes;
                //logTime = 5;
                if (logTime >= 1)
                {
                    string ended_at = ended_at_hour;
                    string logged_time = "" + (int)logTime;
                    AppGlobal.start_time_global = DateTime.Now;
                    string token2 = AppGlobal.TokenRes;
                    string responseLogMin = util_api2.LogMinute(logged_time, date, started_at, ended_at, token2, processList);
                    AppGlobal.savelog(responseLogMin, true);
                    AppGlobal.savelog("Log time sent..", true);
                    LogMinuteEntity logmResponse = JsonConvert.DeserializeObject<LogMinuteEntity>(responseLogMin);
                    if (logmResponse.code == 201)
                    {
                        //savelog("Log Minute Api Response...: " + logmResponse.messages[0], true);
                        return logmResponse.messages[0];
                    }
                    else
                    {
                        //savelog("Log Minute Api Response...: " + logmResponse.messages[0], true);
                        return logmResponse.messages[0];
                    }
                }
                else
                {
                    return "Not sent 0";
                }
                #endregion
            }
            catch (Exception ex)
            {
                AppGlobal.savelog(ex.ToString(), false);
                return "exception";
            }
        }
        public static string PostStatus(String type)
        {

            try
            {

                #region Post Status
                string time = DateTime.Now.ToString("HH:mm", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                string date = Convert.ToDateTime(DateTime.Now.ToString()).Date.ToString("yyyy-MM-dd");
                string token2 = AppGlobal.TokenRes;
                UtilAPI util_api2 = new UtilAPI();

                string responseLogMin = util_api2.PostStatus(date, time, type, token2);
                PostStatusEntity logmResponse = JsonConvert.DeserializeObject<PostStatusEntity>(responseLogMin);
                if (logmResponse.code == 201)
                {
                    AppGlobal.savelog("Post Status Api Response when " + type + " button click...: " + logmResponse.messages[0], true);
                }
                else
                {
                    AppGlobal.savelog("Post Status Api Response when " + type + " button click...: " + logmResponse.messages[0], true);
                }
                return "Status Sent";
                #endregion
            }
            catch (Exception ex)
            {
                AppGlobal.savelog(ex.ToString(), false);
                return "exception";
            }
        }
        public static void savelog(string log_data, bool is_server)
        {
            String ser_log = "SerLogs";
            String err_log = "ErrorLogs";
            String absolute_path = "";
            try
            {
                String month_date = DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString();
                if (is_server)
                {
                    absolute_path = AppGlobal.log_path + "//" + month_date + "//" + ser_log + "//";
                }
                else
                {
                    absolute_path = AppGlobal.log_path + "//" + month_date + "//" + err_log + "//";
                }
                bool exists = Directory.Exists(absolute_path);
                if (!exists)
                    Directory.CreateDirectory(absolute_path);
                FileStream fp = new FileStream(absolute_path + DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + "_log.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fp);
                sw.WriteLine(DateTime.Now.ToString() + " ==> " + log_data);
                sw.Close();
                fp.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error on save log" + ex.ToString());
            }
        }
    }
}
