﻿using RestSharp;
using RestSharp.Extensions.MonoHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WFH
{
    class UtilAPI
    {
        #region API Collection
        public String LoginToHRIS(String uid, String pass)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                byte[] buf = new byte[8192];
                String args = "?emp_id=" + uid.Trim() + "&password=" + HttpUtility.UrlEncode(pass.Trim());
                //AppGlobal.savelog(AppGlobal.script_url_login + args, true);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AppGlobal.script_url_login + args);
                AppGlobal.savelog("Setting time out : " + AppGlobal.time_out, true);
                request.Timeout = Convert.ToInt32(AppGlobal.time_out);
                request.Method = "POST";
                request.ContentType = "application/form-data";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream resStream = response.GetResponseStream();
                string tempString = null;
                int count = 0;
                do
                {
                    count = resStream.Read(buf, 0, buf.Length);
                    if (count != 0)
                    {
                        tempString = Encoding.ASCII.GetString(buf, 0, count);
                        sb.Append(tempString);
                    }
                }
                while (count > 0);
            }
            catch (Exception err)
            {
                AppGlobal.savelog(err.ToString(), false);
                return "Failed";
            }
            string response_str = sb.ToString();
            return response_str;

        }

        public String AgentInstalled(string token)
        {
            var client = new RestClient(AppGlobal.script_url_agent_installed);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public String FileUpload(String token, String files)
        {

            var client = new RestClient(AppGlobal.script_url_file_uload);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Content-Type", "multipart/form-data");
            request.AddFile("file", files);
            request.AddParameter("type", "track");
            IRestResponse response = client.Execute(request);
            client.ClearHandlers();
            return response.Content;
        }
        public class ListProcess
        {

        }
        public String LogMinute(string logged_time, string date, string started_at, string ended_at, string token, List<string> processList)
        {
            string pattern = "";
            processList.ForEach(f =>
            {
                pattern = pattern + f + ",";

            });
            pattern = pattern.Replace("\\","");
            var client = new RestClient(AppGlobal.script_url_log_time);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "{\n\t\"logged_time\" : \"" + logged_time + "\",\n\t\"date\": \"" + date + "\",\n\t\"started_at\" : \"" + started_at + "\",\n\t\"ended_at\" : \"" + ended_at + "\",\n\t\"process_list\" : [\"" + pattern + "\"]\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public String PostStatus(string date, string time, string type, string token)
        {
            var client = new RestClient(AppGlobal.script_url_post_status);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "{\n\t\"date\": \"" + date + "\",\n\t\"time\" : \"" + time + "\",\n\t\"type\" : \"" + type + "\"\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public String LogTime()
        {
            var client = new RestClient(AppGlobal.script_url_looged_time);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + AppGlobal.TokenRes);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        public string EligibilityChecking(string token3)
        {
            var client = new RestClient(AppGlobal.script_url_leligibility);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + token3);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        #endregion

    }
}
