﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFH.Entity
{
    public class FileUploadEntity
    {
        public int code { get; set; }
        public string[] messages { get; set; }
        public DataCollection data { get; set; }
    }
    public class DataCollection
    {
        public string type{ get; set; }
        public string name { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
        public string id { get; set; }

    }
}
