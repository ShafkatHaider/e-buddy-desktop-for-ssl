﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFH.Entity
{
    public class PostStatusEntity
    {
        public int code { get; set; }
        public string[] messages { get; set; }
        public DataCollectionPost data { get; set; }
    }
    public class DataCollectionPost
    {
        public string emp_id { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string type { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
        public string id { get; set; }

    }
}
