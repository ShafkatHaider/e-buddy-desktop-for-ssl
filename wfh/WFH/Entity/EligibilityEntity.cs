﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFH.Entity
{
    public class EligibilityEntity
    {
        public int code { get; set; }
        public string[] messages { get; set; }
        public DataCollectionEligibility data { get; set; }
    }
    public class DataCollectionEligibility
    {
        public string  id { get; set; }
        public string emp_id { get; set; }
        public string workstation_image_uploaded { get; set; }
        public string work_station_image { get; set; }
        public string desktop_agent_installed { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
    }
    }
