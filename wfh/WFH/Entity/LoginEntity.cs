﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFH.Entity
{

    public class LoginEntity
    {
        public int code { get; set; }
        public string[] messages { get; set; }
        public Data data { get; set; }
    }

    public class Data
    {
        public User user { get; set; }
        public string token { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string emp_id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string contact_no { get; set; }
        public string department_head { get; set; }
        public string reporting_to { get; set; }
        public string employee_category { get; set; }
        public object category_expire_date { get; set; }
        public string check_attendance { get; set; }
        public string status { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string designation { get; set; }
        public string department { get; set; }
        public object profile_pic { get; set; }
    }

}