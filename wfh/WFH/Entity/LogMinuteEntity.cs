﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFH.Entity
{
    public class LogMinuteEntity
    {
        public int code { get; set; }
        public string[] messages { get; set; }
        public DataCollectionLog data { get; set; }
    }
    public class DataCollectionLog
    {
        public string emp_id { get; set; }
        public string date { get; set; }
        public string started_at { get; set; }
        public string ended_at { get; set; }
        public string logged_time { get; set; }
        public string updated_at { get; set; }
        public string created_at { get; set; }
        public string id { get; set; }

    }
}
