﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFH
{
    public partial class DialogBox : Form
    {
        public DialogBox(String img_url)
        {
            InitializeComponent();
            Image img=Image.FromFile(img_url);
            pictureBox_sc.Image = img;
        }
        private bool okButton = false;

        public bool OKButtonClicked
        {
            get { return okButton; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            okButton = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            okButton = false;
            this.Close();
        }
    }
}
